/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hotel;

/**
 * @version 1.0
 * @author Hendri Winarto (08)
 */
public class DataHotel {
    public long NoKamar;
    public int tipeKamar;
    public String JenisKamar;
    public long Tarif;
    
    public Object[][] refTarif = {
                                {}, //index ke-0 tidak digunakan
                                {1, "Double Bed", 700000},
                                {2, "Single Bed", 500000},
                                {3, "Family", 1000000},
                                {4, "President Suite", 1500000},
                               };
}
