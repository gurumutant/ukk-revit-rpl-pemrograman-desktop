/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hotel;

/**
 * @version 1.0
 * @author Hendri Winarto (08)
 */
public class Transaksi {
    public String noTransaksi;
    public String tglReservasi;
    public String noKTP;
    public String noHP;
    public String namaPelanggan;
    public String CheckIn;
    public String CheckOut;
    public long deposit;
    public long totalTagihan;
    public long biayaTambahan;
}
